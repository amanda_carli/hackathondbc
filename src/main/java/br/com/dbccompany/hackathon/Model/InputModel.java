package br.com.dbccompany.hackathon.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class InputModel {

    private String campo1;
    private String campo2;
    private String campo3;
    private String campo4;

}
