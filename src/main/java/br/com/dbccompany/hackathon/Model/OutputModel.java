package br.com.dbccompany.hackathon.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OutputModel {

    private int countClientes;
    private int countVendedores;
    private String idVendaMaisCara;
    private String nomePiorVendedor;

}
