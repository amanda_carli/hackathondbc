package br.com.dbccompany.hackathon.Builder;

import br.com.dbccompany.hackathon.Model.VendasModel;

public class VendasBuilder {
    public VendasModel run(String[] s) {
        return new VendasModel(s[1], s[2], s[3]);
    }
}
