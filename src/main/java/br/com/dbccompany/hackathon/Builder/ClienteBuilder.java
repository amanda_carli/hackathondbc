package br.com.dbccompany.hackathon.Builder;


import br.com.dbccompany.hackathon.Model.ClienteModel;

public class ClienteBuilder {

    public ClienteModel run(String[] s) {
        return new ClienteModel(s[1], s[2], s[3]);
    }
}
